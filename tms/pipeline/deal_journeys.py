from tms.pipeline.interface import Pipeline


pipeline = Pipeline(
    "deal_journeys",
    """
    SELECT
        id,
        deal_id,
        create_date,
        content,
        created_at,
        updated_at
    FROM
        deal_journeys
    WHERE updated_at BETWEEN %(start)s AND %(end)s
    """,
    [
        {"name": "id", "type": "NUMERIC"},
        {"name": "deal_id", "type": "NUMERIC"},
        {"name": "create_date", "type": "TIMESTAMP"},
        {"name": "content", "type": "STRING"},
        {"name": "created_at", "type": "TIMESTAMP"},
        {"name": "updated_at", "type": "TIMESTAMP"},
    ],
)
