from tms.pipeline.interface import Pipeline

pipeline = Pipeline(
    "operators",
    """
    SELECT
        id,
        name,
        description,
        created_at,
        updated_at,
        logo
    FROM
        operators
    WHERE updated_at BETWEEN %(start)s AND %(end)s
    """,
    [
        {"name": "id", "type": "NUMERIC"},
        {"name": "name", "type": "STRING"},
        {"name": "description", "type": "STRING"},
        {"name": "created_at", "type": "TIMESTAMP"},
        {"name": "updated_at", "type": "TIMESTAMP"},
        {"name": "logo", "type": "STRING"},
    ],
)
