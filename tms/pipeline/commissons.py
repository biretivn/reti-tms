from tms.pipeline.interface import Pipeline

pipeline = Pipeline(
    "commissions",
    """
    SELECT
        id,
        deal_id,
        created_at,
        updated_at,
        calculation_type,
        cross_selling_agent_code,
        applicable_policy,
        seller_type,
        reti_commission_rate,
        reti_actually_rate,
        reti_commission_rate_for_seller,
        seller_commission_rate_will_be_received,
        seller_commission_rate_from_reti,
        price_to_calculate_commission_for_seller,
        temporary_commission,
        actually_commission
    FROM
        commissions
    WHERE updated_at BETWEEN %(start)s AND %(end)s
    """,
    [
        {"name": "id", "type": "NUMERIC"},
        {"name": "deal_id", "type": "NUMERIC"},
        {"name": "created_at", "type": "TIMESTAMP"},
        {"name": "updated_at", "type": "TIMESTAMP"},
        {"name": "calculation_type", "type": "NUMERIC"},
        {"name": "cross_selling_agent_code", "type": "STRING"},
        {"name": "applicable_policy", "type": "NUMERIC"},
        {"name": "seller_type", "type": "NUMERIC"},
        {"name": "reti_commission_rate", "type": "NUMERIC"},
        {"name": "reti_actually_rate", "type": "NUMERIC"},
        {"name": "reti_commission_rate_for_seller", "type": "NUMERIC"},
        {"name": "seller_commission_rate_will_be_received", "type": "NUMERIC"},
        {"name": "seller_commission_rate_from_reti", "type": "NUMERIC"},
        {"name": "price_to_calculate_commission_for_seller", "type": "NUMERIC"},
        {"name": "temporary_commission", "type": "NUMERIC"},
        {"name": "actually_commission", "type": "NUMERIC"},
    ],
)
