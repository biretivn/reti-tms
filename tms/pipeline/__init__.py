from tms.pipeline import (
    deals,
    developments,
    commissons,
    constructors,
    customers,
    customer_personas,
    deal_journeys,
    deal_logs,
    investors,
    operators,
    projects,
    users,
    products,
    transfer_histories,
)

pipelines = {
    i.table: i
    for i in [
        j.pipeline
        for j in [
            deals,
            commissons,
            customers,
            projects,
            users,
            customer_personas,
            investors,
            deal_journeys,
            deal_logs,
            products,
            constructors,
            developments,
            operators,
            transfer_histories,
        ]
    ]
}
