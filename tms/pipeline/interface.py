from typing import Callable, Any, Optional
from dataclasses import dataclass


@dataclass
class Pipeline:
    table: str
    sql: str
    schema: list[dict[str, Any]]
    transform: Callable[
        [list[dict[str, Any]]],
        list[dict[str, Any]],
    ] = lambda rows: [{k: transform(v) for k, v in row.items()} for row in rows]
    cursor_key: str = "updated_at"
    id_key: str = "id"


def transform(x: Optional[Any]) -> Any:
    if isinstance(x, list):
        return _transform_array(x)
    else:
        return x


def _transform_array(x: Optional[list[Any]]) -> list[Any]:
    return [y for y in x if y] if x else []
