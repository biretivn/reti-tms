from tms.pipeline.interface import Pipeline


pipeline = Pipeline(
    "transfer_histories",
    """
    SELECT
        id,
        deal_id,
        transferred_id,
        created_at,
        updated_at
    FROM
        transfer_histories
    WHERE updated_at BETWEEN %(start)s AND %(end)s
    """,
    [
        {"name": "id", "type": "NUMERIC"},
        {"name": "deal_id", "type": "NUMERIC"},
        {"name": "transferred_id", "type": "NUMERIC"},
        {"name": "created_at", "type": "TIMESTAMP"},
        {"name": "updated_at", "type": "TIMESTAMP"},
    ],
)
