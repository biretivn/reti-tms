from typing import Any
import os

import psycopg2
import psycopg2.extras
from dateutil.parser import parse

from db.utils import TZ

TIMESTAMP = psycopg2.extensions.new_type(
    (1114,),
    "timestamp",
    lambda value, _: parse(value).replace(tzinfo=TZ).isoformat(timespec="seconds")
    if value
    else None,
)
psycopg2.extensions.register_type(TIMESTAMP)

DATE = psycopg2.extensions.new_type(
    (1082,),
    "date",
    lambda value, _: parse(value).date().isoformat() if value else None,
)
psycopg2.extensions.register_type(DATE)

NUMERIC = psycopg2.extensions.new_type(
    (1700,),
    "date",
    lambda value, _: float(value) if value else None,
)
psycopg2.extensions.register_type(NUMERIC)


def get_connection():
    return psycopg2.connect(
        dbname="reti_production_ver2",
        host=os.getenv("PG_HOST", ""),
        user=os.getenv("PG_USER"),
        password=os.getenv("PG_PWD"),
        cursor_factory=psycopg2.extras.RealDictCursor,
    )


def query(sql: str):
    def _query(params: dict[str, Any]) -> list[dict[str, Any]]:
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute(sql, params)
                return cur.fetchall()

    return _query
